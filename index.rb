require 'sinatra'
require 'mysql2'

set :public_folder, File.dirname(__FILE__) + '/static'

client = nil

before do
    client = Mysql2::Client.new(
        :host => 'localhost', 
        :username => 'root',
        :database => 'order'
        )
end

get '/' do
    erb :index
end

get '/detail' do

    erb :detail
end

get '/checked' do
    
    erb :checked
    # erb :checked, :locals => { :text => 'hello world' }
end


get '/admin/login' do

    erb :login
end

get '/admin/list' do

    results = client.query("SELECT * FROM `dishes` ORDER BY `id` DESC")
    # results.each do |row|
    #     puts row['name']
    # end
    erb :list, :locals => { :results => results }
end

get '/admin/add' do
    erb :add
end

post '/admin/add/do' do

    if params['food_name'] == ''
        erb 'Food name is empty'
    else
        client.query("INSERT INTO `dishes`(`name`, `price`, `duration`, `meterials`, `process`) 
                    VALUES(
                        '#{params['food_name']}',
                        '#{params['food_price']}',
                        '#{params['food_duration']}',
                        '#{params['food_material']}',
                        '#{params['food_process']}'
                        )")

        redirect to('/admin/list')
    end
   
    
end








# results = client.query("select * from runoob_tbl")
    # results = client.query("insert into runoob_tbl(`runoob_id`, `runoob_title`, `runoob_author`, `submission_date`) values(4444, 'xxxx', 'zzzzz', '2019-08-22')")
    #results = client.query("UPDATE `runoob_tbl` SET `runoob_title`='whoryou' WHERE `runoob_id`=333")
  
    # headers = results.fields
    # puts headers
    # results.each do |row|
    #     puts row
    # end




